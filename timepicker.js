var myApp = angular.module('myApp',['angularMoment']);

function MyCtrl($scope) {
    $scope.name = 'Superhero';
}

myApp.directive('tcTimepicker', function () {
    return {
        restrict: "E",
        scope: {

        },
        replace: true,
        templateUrl: 'directive.tpl.html',
        link: function (scope) {

            scope.$timeout(function () {
                scope.mainElement.addClass('ready');
            });

            scope.modify();
            scope.setSelection(scope.rotation);
        },
        controller: function ($scope, $element, $timeout, moment) {

            var moveTimeout;

            $scope.$timeout = $timeout;
            $scope.$moment = moment;

            $scope.mainElement = $element;
            $scope.element = $element[0].querySelector('.timepicker__carousel');
            $scope.controlBtns = $element[0].querySelectorAll('.timepicker__controls');
            $scope.items = $scope.element.querySelectorAll('figure');
            $scope.rotation = 0;
            $scope.totalPanelCount = $scope.element.children.length;
            $scope.panelCount = $scope.totalPanelCount;
            $scope.isHorizontal = false;
            $scope.panelSize = $scope.element[ $scope.isHorizontal ? 'offsetWidth' : 'offsetHeight' ];
            $scope.rotateFn = $scope.isHorizontal ? 'rotateY' : 'rotateX';
            $scope.theta = 360 / $scope.panelCount;
            $scope.panelMargin = 20;

            //Generate hours with moment
            // for (var i = 0, l = 24; i < l; i++) {
            //     $scope.items.push($scope.$moment().startOf('day').add(i, 'hours').format('HH:mm'));
            // }

            function round(value, precision) {
                var multiplier = Math.pow(10, precision || 0);
                return Math.round(value * multiplier) / multiplier;
            }

            $scope.setSelection = function (angle) {
                angular.forEach($scope.items, function(value, key) {
                    angular.element(value).removeClass('selected');

                    if(value.style['transform'].indexOf(Math.abs(round(angle, 2)) + 'deg') !== -1) {
                        angular.element(value).addClass('selected');
                    }
                });
            };

            $scope.modify = function() {

                var panel, angle, i;

                // do some trig to figure out how big the carousel
                // is in 3D space
                $scope.radius = Math.round( ( $scope.panelSize / $scope.panelCount) / Math.tan( Math.PI / $scope.panelCount )) + (($scope.panelCount / 2) * 20);

                for ( i = 0; i < $scope.panelCount; i++ ) {
                    panel = $scope.element.children[i];
                    angle = $scope.theta * i;
                    panel.style.opacity = 1;

                    // rotate panel, then push it out in 3D space
                    panel.style['transform'] = $scope.rotateFn + '(' + round(angle, 2) + 'deg) translateZ(' + $scope.radius + 'px)';
                }

                // hide other panels
                for (  ; i < $scope.totalPanelCount; i++ ) {
                    panel = $scope.element.children[i];
                    panel.style.opacity = 0;
                    panel.style['transform'] = 'none';
                }

                // adjust rotation so panels are always flat
                $scope.rotation = Math.round( $scope.rotation / $scope.theta ) * $scope.theta;

                $scope.transform();

            };

            $scope.transform = function(callback) {
                // push the carousel back in 3D space,
                // and rotate it
                $scope.element.style['transform'] = 'translateZ(-' + $scope.radius + 'px) ' + $scope.rotateFn + '(' + $scope.rotation + 'deg)';

                if(typeof callback === 'function') {
                    callback();
                }
            };

            $element.on('touchstart', function (event) {
                var startY = event.touches[0].clientY,
                    delta,
                    increment;

                $element.off('touchmove');
                $element.on('touchmove', function (event) {

                    if (moveTimeout) {
                        // clear the timeout, if one is pending
                        clearTimeout(moveTimeout);
                        moveTimeout = null;
                    }
                    moveTimeout = setTimeout(function () {
                        delta = event.touches[0].clientY - startY;
                        increment = delta < 0 ? -1 : 1;
                        $scope.rotation += $scope.theta * increment * -1;
                        $scope.rotationCheck = $scope.rotation % 360;

                        if (Math.round($scope.rotationCheck) > 0) {
                            $scope.rotationCheck -= 360;
                        }

                        $scope.transform($scope.setSelection($scope.rotationCheck));
                    }, 100);
                });
            });

            angular.forEach($scope.controlBtns, function(value, key) {
                angular.element(value).on('click', function (event) {
                    var increment = parseInt(event.target.getAttribute('data-increment')),
                        incrementValue = $scope.theta * increment * -1;

                    $scope.rotation += incrementValue;
                    $scope.rotationCheck = $scope.rotation % 360;

                    if (Math.round($scope.rotationCheck) > 0) {
                        $scope.rotationCheck -= 360;
                    }

                    $scope.transform($scope.setSelection($scope.rotationCheck));
                });
            });
        }
    }
});